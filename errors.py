class CounterResetError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __repr__(self):
        return repr(self.msg)

    def __str__(self):
        return self.msg


class ReadError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __repr__(self):
        return repr(self.msg)

    def __str__(self):
        return self.msg


class EmptyReadError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __repr__(self):
        return repr(self.msg)

    def __str__(self):
        return self.msg


class RetriesExceededError(Exception):
    def __init__(self, n):
        self.msg = 'exceeded maximum number of %d retries' % n

    def __repr__(self):
        return repr(self.msg)

    def __str__(self):
        return self.msg
