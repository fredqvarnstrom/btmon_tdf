# The day of week to upload sensor data to remote server
# Caution: Case sensitive!!!

MINUTE = 60
SpH = 3600.0

# if set to 1, print out what would be uploaded but do not do the upload.
SKIP_UPLOAD = 0

# if set to 1, trust the device's clock
TRUST_DEVICE_CLOCK = 0

# if set to 1, obfuscate any serial number before uploading
OBFUSCATE_SERIALS = 1

# brultech assumes that positive polarized watt-seconds means negative energy,
# resulting in this calculation for watt-seconds:
#   ws = aws - 2*pws
# btmon assumes that positive polarized watt-seconds means positive energy,
# resulting in this calculation for watt-seconds:
#   ws = 2*pws - aws
# when reverse polarity is specified, the sign is inverted for each channel
# that has a polarized watt-second counter so that the power and energy values
# match those of brultech software.
REVERSE_POLARITY = 1

# number of retries to attempt when reading device, 0 means retry forever
READ_RETRIES = 0

# how long to wait after a failure before attempting to read again, in seconds
RETRY_WAIT = 60

# number of retries to attempt when polling device
POLL_RETRIES = 3

# size of the rolling buffer into which data are cached
# should be at least max(upload_period) / sample_period
# a sample period of 10s and max upload period of 15m (900s), with contingency
# of 5m (300s) tdf_server/network downtime yields a buffer of 120
DEFAULT_BUFFER_SIZE = 120

# how long to wait before considering an upload to have failed, in seconds
DEFAULT_UPLOAD_TIMEOUT = 15

# how often to upload data, in seconds
# this may be overridden by specific services
DEFAULT_UPLOAD_PERIOD = 15 * MINUTE

# device types
DEV_ECM1220 = 'ecm1220'
DEV_ECM1240 = 'ecm1240'
DEV_GEM = 'gem'
DEVICE_TYPES = [DEV_ECM1220, DEV_ECM1240, DEV_GEM]
DEFAULT_DEVICE_TYPE = DEV_GEM

# packet formats
PF_ECM1220BIN = 'ecm1220bin'  # ECM-1220 binary
PF_ECM1240BIN = 'ecm1240bin'  # ECM-1240 binary
PF_GEM48PTBIN = 'gem48ptbin'  # GEM 48-channel binary with polarity, timestamp
PF_GEM48PBIN = 'gem48pbin'    # GEM 48-channel binary with polarity
PACKET_FORMATS = [PF_ECM1220BIN, PF_ECM1240BIN, PF_GEM48PTBIN, PF_GEM48PBIN]

# the database schema
DB_SCHEMA_COUNTERS = 'counters'  # just the counters and sensor readings
DB_SCHEMA_ECMREAD = 'ecmread'  # basic format used by ecmread
DB_SCHEMA_ECMREADEXT = 'ecmreadext'  # extended format used by ecmread
DB_SCHEMAS = [DB_SCHEMA_COUNTERS, DB_SCHEMA_ECMREAD, DB_SCHEMA_ECMREADEXT]
DEFAULT_DB_SCHEMA = DB_SCHEMA_COUNTERS

# channel filters
FILTER_PE_LABELS = 'pelabels'
FILTER_POWER = 'power'
FILTER_ENERGY = 'energy'
FILTER_PULSE = 'pulse'
FILTER_SENSOR = 'sensor'
FILTER_DB_SCHEMA_COUNTERS = DB_SCHEMA_COUNTERS
FILTER_DB_SCHEMA_ECMREAD = DB_SCHEMA_ECMREAD
FILTER_DB_SCHEMA_ECMREADEXT = DB_SCHEMA_ECMREADEXT

# serial settings
# the serial port to which device is connected e.g. COM4, /dev/ttyS01
SERIAL_PORT = '/dev/ttyUSB0'
SERIAL_BAUD = 115200
SERIAL_BUFFER_SIZE = 2048

# ethernet settings
# the etherbee defaults to pushing data to port 8083
# the wiz110rs defaults to listening on port 5000
IP_SERVER_HOST = ''  # bind to default
IP_SERVER_PORT = 8083
IP_CLIENT_PORT = 5000
IP_CLIENT_TIMEOUT = 60
IP_DEFAULT_MODE = 'tdf_server'
IP_BUFFER_SIZE = 2048

# database defaults
DB_HOST = 'localhost'
DB_USER = 'ecmuser'
DB_PASSWD = 'ecmpass'
DB_DATABASE = 'ecm'
DB_FILENAME = 'ecm.sqlite'  # filename for sqlite databases
DB_INSERT_PERIOD = MINUTE  # how often to record to database, in seconds
DB_POLL_INTERVAL = 60  # how often to poll the database, in seconds

# rrd defaults
RRD_DIR = 'rrd'  # directory in which to put the rrd files
RRD_STEP = 10  # how often we get samples from the device, in seconds
RRD_HEARTBEAT = 2 * RRD_STEP  # typically twice the step, in seconds
# 10s, 5m, 30m, 1h
# 4d at 10s, 60d at 5m, 365d at 30m, 730d at 1h
# 2087836 bytes per rrd file - about 90MB for a gem or 20MB for an ecm1240
RRD_STEPS = [1, 18, 180, 360]
RRD_RESOLUTIONS = [34560, 17280, 17520, 17520]
# 10s, 1m, 10m, 20m
# 32h at 10s, 12d at 1m, 121.6d at 10m, 243.3d at 20m
# 30s, 5m, 30m, 1h
# 4d at 30s, 60d at 5m, 365d at 30m, 730d at 1h
# 1534876 bytes per rrd file - about 75MB for a gem or 15MB for an ecm1240
# RRD_STEPS = [1,6,60,120]
# RRD_RESOLUTIONS = [11520, 17280, 17520, 17520]
RRD_UPDATE_PERIOD = 60  # how often to update the rrd files, in seconds
RRD_POLL_INTERVAL = 120  # how often to poll when rrd is source, in seconds

# WattzOn defaults
# the map is a comma-delimited list of channel,meter pairs.  for example:
#   311111_ch1,living room,311112_ch1,parlor,311112_aux4,kitchen
WATTZON_API_URL = 'http://www.wattzon.com/api/2009-01-27/3'
WATTZON_UPLOAD_PERIOD = 5 * MINUTE
WATTZON_TIMEOUT = 15  # seconds
WATTZON_MAP = ''
WATTZON_API_KEY = 'apw6v977dl204wrcojdoyyykr'
WATTZON_USER = ''
WATTZON_PASS = ''

# PlotWatt defaults
#   https://plotwatt.com/docs/api
#   Recommended upload period is one minute to a few minutes.  Recommended
#   sampling as often as possible, no faster than once per second.
# the map is a comma-delimited list of channel,meter pairs.  for example:
#   311111_ch1,1234,311112_ch1,1235,311112_aux4,1236
PLOTWATT_BASE_URL = 'http://plotwatt.com'
PLOTWATT_UPLOAD_URL = '/api/v2/push_readings'
PLOTWATT_UPLOAD_PERIOD = MINUTE
PLOTWATT_TIMEOUT = 15  # seconds
PLOTWATT_MAP = ''
PLOTWATT_HOUSE_ID = ''
PLOTWATT_API_KEY = ''

# EnerSave defaults
#   Minimum upload interval is 60 seconds.
#   Recommended sampling interval is 2 to 30 seconds.
# the map is a comma-delimited list of channel,description,type tuples
#   311111_ch1,living room,2,311112_ch2,solar,1,311112_aux4,kitchen,2
ES_URL = 'http://data.myenersave.com/fetcher/data'
ES_UPLOAD_PERIOD = 5 * MINUTE
ES_TIMEOUT = 60  # seconds
ES_TOKEN = ''
ES_MAP = ''
ES_DEFAULT_TYPE = 2

# Bidgely defaults
#   Minimum upload interval is 60 seconds.
#   Recommended sampling interval is 2 to 30 seconds.
# the map is a comma-delimited list of channel,description,type tuples
#   311111_ch1,living room,2,311112_ch2,solar,1,311112_aux4,kitchen,2
BY_UPLOAD_PERIOD = 60  # seconds
BY_TIMEOUT = 60  # seconds
BY_MAP = ''
BY_DEFAULT_TYPE = 10

# PeoplePower defaults
#   http://developer.peoplepowerco.com/docs
#   Recommended upload period is 15 minutes.
# the map is a comma-delimited list of channel,meter pairs.  for example:
#   311111_ch1,1111c1,311112_ch1,1112c1,311112_aux4,1112a4
PPCO_URL = 'https://esp.peoplepowerco.com:8443/deviceio/ml'
PPCO_UPLOAD_PERIOD = 15 * MINUTE
PPCO_TIMEOUT = 15  # seconds
PPCO_TOKEN = ''
PPCO_HUBID = 1
PPCO_MAP = ''
PPCO_FIRST_NONCE = 1
PPCO_DEVICE_TYPE = 1005
PPCO_ADD_DEVICES = True  # set to false to skip setup if setup already done

# eragy defaults
ERAGY_URL = 'http://d.myeragy.com/energyremote.aspx'
ERAGY_UPLOAD_PERIOD = MINUTE
ERAGY_TIMEOUT = 15  # seconds
ERAGY_GATEWAY_ID = ''
ERAGY_TOKEN = ''

# smart energy groups defaults
#   http://smartenergygroups.com/api
# the map is a comma-delimited list of channel,meter pairs.  for example:
#   311111_ch1,living room,311112_ch1,parlor,311112_aux4,kitchen
SEG_URL = 'http://api.smartenergygroups.com/api_sites/stream'
SEG_UPLOAD_PERIOD = MINUTE
SEG_TIMEOUT = 15  # seconds
SEG_TOKEN = ''
SEG_MAP = ''

# thingspeak defaults
#   http://community.thingspeak.com/documentation/api/
#   Uploads are limited to no more than every 15 seconds per channel.
TS_URL = 'http://api.thingspeak.com/update'
TS_UPLOAD_PERIOD = MINUTE
TS_TIMEOUT = 15  # seconds
TS_TOKENS = ''
TS_FIELDS = ''

# pachube/cosm defaults
#   https://cosm.com/docs/v2/
PBE_URL = 'http://api.xively.com/v2/feeds'
PBE_UPLOAD_PERIOD = MINUTE
PBE_TIMEOUT = 15  # seconds
PBE_TOKEN = ''
PBE_FEED = ''

# open energy monitor emoncms defaults
OEM_URL = 'https://localhost/emoncms/api/post'
OEM_UPLOAD_PERIOD = MINUTE
OEM_TIMEOUT = 15  # seconds
OEM_TOKEN = ''
OEM_NODE = None

# wattvision v0.2 defaults
#   https://www.wattvision.com/usr/api
#   post as fast as every 15 seconds or as slow as every 20 minutes.
WV_URL = 'http://www.wattvision.com/api/v0.2/elec'
WV_UPLOAD_PERIOD = 120
WV_TIMEOUT = 15  # seconds
WV_API_ID = ''
WV_API_KEY = ''
WV_SENSOR_ID = ''
WV_CHANNEL = ''

# pvoutput defaults
#   http://www.pvoutput.org/help.html
#   using the addstatus interface
#   send a sample every 5 to 15 minutes
#   use the cumulative flag for energy
PVO_URL = 'http://pvoutput.org/service/r2/addstatus.jsp'
PVO_UPLOAD_PERIOD = 300
PVO_TIMEOUT = 15  # seconds
PVO_API_KEY = ''
PVO_SYSTEM_ID = ''
PVO_GEN_CHANNEL = ''
PVO_CON_CHANNEL = ''
PVO_TEMP_CHANNEL = ''
