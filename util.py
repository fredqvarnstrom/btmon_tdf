import time

from constant import *

# logging and error reporting
#
# note that setting the log level to debug will affect the application
# behavior, especially when sampling the serial line, as it changes the
# timing of read operations.
LOG_ERROR = 0
LOG_WARN = 1
LOG_INFO = 2
LOG_DEBUG = 3
LOGLEVEL = 3


def dbgmsg(msg):
    if LOGLEVEL >= LOG_DEBUG:
        logmsg(msg)


def infmsg(msg):
    if LOGLEVEL >= LOG_INFO:
        logmsg(msg)


def wrnmsg(msg):
    if LOGLEVEL >= LOG_WARN:
        logmsg(msg)


def errmsg(msg):
    if LOGLEVEL >= LOG_ERROR:
        logmsg(msg)


def logmsg(msg):
    ts = fmttime(time.localtime())
    print "%s %s" % (ts, msg)


# Helper Functions

def fmttime(seconds):
    return time.strftime("%Y/%m/%d %H:%M:%S", seconds)


def mkts(seconds):
    return time.strftime('%Y-%m-%dT%H:%M:%SZ', time.gmtime(seconds))


def getgmtime():
    return int(time.time())


def cleanvalue(s):
    """ensure that values read from configuration file are sane"""
    s = s.replace('\n', '')  # we never want newlines
    s = s.replace('\r', '')  # or carriage returns
    if s.lower() == 'false':
        s = False
    elif s.lower() == 'true':
        s = True
    return s


def pairs2dict(s):
    """convert comma-delimited name,value pairs to a dictionary"""
    items = s.split(',')
    m = dict()
    for k, v in zip(items[::2], items[1::2]):
        m[k] = v
    return m


def obfuscate_serial(sn):
    """obfuscate a serial number - expose only the last 3 digits"""
    if OBFUSCATE_SERIALS:
        n = len(sn)
        s = 'XXX%s' % sn[n - 3:n]
    else:
        s = sn
    return s


def mklabel(sn, channel):
    return '%s_%s' % (sn, channel)


def mkfn(_dir, label):
    label = label.replace(' ', '_')
    label = label.replace(':', '_')
    label = label.replace('/', '_')
    return '%s/%s.rrd' % (_dir, label)
