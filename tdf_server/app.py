"""
    RESTful sever built with Flask.
"""
import os

from flask import Flask, jsonify, request
from flask.ext.sqlalchemy import SQLAlchemy
import zipfile
from werkzeug import secure_filename


app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:postgres@localhost/tdf'

db = SQLAlchemy(app)
token = 'btmon-tdf-aeb3-44c2-86bd-bb3ee8622db1'


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] == 'zip'


class Result(db.Model):
    __tablename__ = 'results'

    data_id = db.Column(db.String())
    time_created = db.Column(db.BigInteger, primary_key=True)
    serial = db.Column(db.String())
    secs = db.Column(db.BigInteger)
    volts = db.Column(db.Float)
    ch1_aws = db.Column(db.BigInteger)
    ch2_aws = db.Column(db.BigInteger)
    ch3_aws = db.Column(db.BigInteger)
    ch4_aws = db.Column(db.BigInteger)
    ch5_aws = db.Column(db.BigInteger)
    ch6_aws = db.Column(db.BigInteger)
    ch7_aws = db.Column(db.BigInteger)
    ch8_aws = db.Column(db.BigInteger)
    ch9_aws = db.Column(db.BigInteger)
    ch10_aws = db.Column(db.BigInteger)
    ch11_aws = db.Column(db.BigInteger)
    ch12_aws = db.Column(db.BigInteger)
    ch13_aws = db.Column(db.BigInteger)
    ch14_aws = db.Column(db.BigInteger)
    ch15_aws = db.Column(db.BigInteger)
    ch16_aws = db.Column(db.BigInteger)
    ch17_aws = db.Column(db.BigInteger)
    ch18_aws = db.Column(db.BigInteger)
    ch19_aws = db.Column(db.BigInteger)
    ch20_aws = db.Column(db.BigInteger)
    ch21_aws = db.Column(db.BigInteger)
    ch22_aws = db.Column(db.BigInteger)
    ch23_aws = db.Column(db.BigInteger)
    ch24_aws = db.Column(db.BigInteger)
    ch25_aws = db.Column(db.BigInteger)
    ch26_aws = db.Column(db.BigInteger)
    ch27_aws = db.Column(db.BigInteger)
    ch28_aws = db.Column(db.BigInteger)
    ch29_aws = db.Column(db.BigInteger)
    ch30_aws = db.Column(db.BigInteger)
    ch31_aws = db.Column(db.BigInteger)
    ch32_aws = db.Column(db.BigInteger)

    def __init__(self, data_id, time_created, serial, secs, volts, ch1_aws, ch2_aws, ch3_aws, ch4_aws, ch5_aws, ch6_aws,
                 ch7_aws, ch8_aws, ch9_aws, ch10_aws, ch11_aws, ch12_aws, ch13_aws, ch14_aws, ch15_aws, ch16_aws,
                 ch17_aws, ch18_aws, ch19_aws, ch20_aws, ch21_aws, ch22_aws, ch23_aws, ch24_aws, ch25_aws, ch26_aws,
                 ch27_aws, ch28_aws, ch29_aws, ch30_aws, ch31_aws, ch32_aws):
        self.data_id = data_id
        self.time_created = time_created
        self.serial = serial
        self.secs = secs
        self.volts = volts
        self.ch1_aws = ch1_aws
        self.ch2_aws = ch2_aws
        self.ch3_aws = ch3_aws
        self.ch4_aws = ch4_aws
        self.ch5_aws = ch5_aws
        self.ch6_aws = ch6_aws
        self.ch7_aws = ch7_aws
        self.ch8_aws = ch8_aws
        self.ch9_aws = ch9_aws
        self.ch10_aws = ch10_aws
        self.ch11_aws = ch11_aws
        self.ch12_aws = ch12_aws
        self.ch13_aws = ch13_aws
        self.ch14_aws = ch14_aws
        self.ch15_aws = ch15_aws
        self.ch16_aws = ch16_aws
        self.ch17_aws = ch17_aws
        self.ch18_aws = ch18_aws
        self.ch19_aws = ch19_aws
        self.ch20_aws = ch20_aws
        self.ch21_aws = ch21_aws
        self.ch22_aws = ch22_aws
        self.ch23_aws = ch23_aws
        self.ch24_aws = ch24_aws
        self.ch25_aws = ch25_aws
        self.ch26_aws = ch26_aws
        self.ch27_aws = ch27_aws
        self.ch28_aws = ch28_aws
        self.ch29_aws = ch29_aws
        self.ch30_aws = ch30_aws
        self.ch31_aws = ch31_aws
        self.ch32_aws = ch32_aws

    def __repr__(self):
        return '<id {}>'.format(self.id)


def handle_response(body, status_code):
    resp = jsonify(body)
    resp.status_code = status_code
    return resp


@app.route('/get_last_time', methods=['POST'])
def get_last_time():
    """
    Get upload time of the last value
    :return:
    """
    try:
        req_param = request.json
    except Exception as e:
        return handle_response({'invalid_header': str(e)}, 400)

    rpi_token = req_param['token']
    sn = req_param['sn']
    if rpi_token != token:
        return handle_response({'code': 'Invalid token'}, 400)
    try:
        last_result = db.session.query(Result).filter(Result.serial == sn).order_by(Result.time_created.desc()).first()
        if last_result is None:
            return handle_response({'last_time': 0}, 200)
        else:
            return handle_response({'last_time': last_result.time_created}, 200)
    except Exception as e:
        return handle_response({'code': str(e)}, 400)


@app.route('/upload_data', methods=['POST'])
def upload_data():
    try:
        _file = request.files['archive']
        print 'Upload request received... {}'.format(_file.filename)
        if _file and allowed_file(filename=_file.filename):
            filename = secure_filename(_file.filename)
            _file.save(filename)
            if append_data(filename):
                os.remove(filename)
                print 'Successfully updated'
                return handle_response({'code': 'OK'}, 200)
            else:
                os.remove(filename)
                print 'Failed to update'
                return handle_response({'code': 'FAIL'}, 200)
    except Exception as er:
        print er
        return handle_response({'code': 'FAIL'}, 200)


def append_data(zip_file):
    """
    Extract zip file and append to data
    :param zip_file: path of zip file
    :return:
    """
    if not zipfile.is_zipfile(zip_file):
        print 'Invalid zip file'
        return False
    zf = zipfile.ZipFile(zip_file)
    try:
        tmp = zf.read('dump.csv').splitlines()
        csv_data = []
        for i in range(1, len(tmp)):
            buf = {}
            for j in range(len(tmp[0].split(','))):
                buf[tmp[0].split(',')[j]] = tmp[i].split(',')[j]
            buf['time_created'] = int(buf['time_created'])
            buf['secs'] = int(buf['secs'])
            buf['volts'] = float(buf['volts'])
            for k in range(1, 33):
                buf['ch{}_aws'.format(k)] = int(buf['ch{}_aws'.format(k)])
            csv_data.append(buf)

        for d in csv_data:
            tb_data = Result(data_id=d['id'], time_created=d['time_created'], serial=d['serial'], secs=d['secs'],
                             volts=d['volts'], ch1_aws=d['ch1_aws'], ch2_aws=d['ch2_aws'], ch3_aws=d['ch3_aws'],
                             ch4_aws=d['ch4_aws'], ch5_aws=d['ch5_aws'], ch6_aws=d['ch6_aws'], ch7_aws=d['ch7_aws'],
                             ch8_aws=d['ch8_aws'], ch9_aws=d['ch9_aws'], ch10_aws=d['ch10_aws'], ch11_aws=d['ch11_aws'],
                             ch12_aws=d['ch12_aws'], ch13_aws=d['ch13_aws'], ch14_aws=d['ch14_aws'],
                             ch15_aws=d['ch15_aws'], ch16_aws=d['ch16_aws'], ch17_aws=d['ch17_aws'],
                             ch18_aws=d['ch18_aws'], ch19_aws=d['ch19_aws'], ch20_aws=d['ch20_aws'],
                             ch21_aws=d['ch21_aws'], ch22_aws=d['ch22_aws'], ch23_aws=d['ch23_aws'],
                             ch24_aws=d['ch24_aws'], ch25_aws=d['ch25_aws'], ch26_aws=d['ch26_aws'],
                             ch27_aws=d['ch27_aws'], ch28_aws=d['ch28_aws'], ch29_aws=d['ch29_aws'],
                             ch30_aws=d['ch30_aws'], ch31_aws=d['ch31_aws'], ch32_aws=d['ch32_aws'])
            if not db.session.query(Result).filter(
                                    Result.time_created == d['time_created'] and Result.serial == d['serial']).count():
                db.session.add(tb_data)
                db.session.commit()
                # print 'Successfully added: {}'.format(d)
            else:
                print 'Failed to append: `SN: {}, time" {}` is already exists'.format(d['time_created'], d['serial'])
        return True

    except KeyError:
        print 'ERROR: Did not find dump.csv in zip file'
        return False


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=8585)

    # append_data('../dump.zip')
