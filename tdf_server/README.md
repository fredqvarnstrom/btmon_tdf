# RESTful server for btnmon_tdf devices
=============================================


## Install dependencies

    sudo apt-get install python-psycopg2 libpq-dev
    sudo pip install -r requirements.txt
    
## Create DB
    
    sudo -u postgres psql
    
  And type this:
    
    createdb tdf
   
  Now, we have to create columns
    
    python
    >>> from app import db
    >>> db.create_all()
    >>> exit()

    
